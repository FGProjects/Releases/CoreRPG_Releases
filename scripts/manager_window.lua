-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function getReadOnlyState(vNode)
	if not DB.isOwner(vNode) then
		return true;
	end
	if DB.isReadOnly(vNode) then
		return true;
	end
	return getLockedState(vNode);
end

function getLockedState(vNode)
	local nDefault = 0;
	if (DB.getModule(vNode) or "") ~= "" then
		nDefault = 1;
	end
	local bLocked = (DB.getValue(DB.getPath(vNode, "locked"), nDefault) ~= 0);
	return bLocked;
end

function getToolbarState(vNode)
	local bToolbar = (DB.getValue(DB.getPath(vNode, "toolbar"), 0) ~= 0);
	return bToolbar;
end
