-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local bInitialized = false;
local bDataFilter = false;
local bTokenFilter = false;

local tModules = {};

function onInit()
	local modules = Module.getModules();
	for _, v in ipairs(modules) do
		checkEntry(v);
	end
	list.applyFilter();
	list.applySort();
	
	Module.onModuleAdded = onModuleAdded;
	Module.onModuleUpdated = onModuleUpdated;
	Module.onModuleRemoved = onModuleRemoved;
	
	bInitialized = true;
end

function onModuleAdded(sModule) checkEntry(sModule); end
function onModuleUpdated(sModule) checkEntry(sModule); end
function onModuleRemoved(sModule) removeEntry(sModule); end

function checkEntry(name)
	if tModules[name] then
		tModules[name].update();
		if bInitialized then
			list.applyFilter();
		end
	else
		addEntry(name);
	end
end

function addEntry(name)
	local w = list.createWindow();
	if w then
		tModules[name] = w;
		w.setEntryName(name);
	end
end

function removeEntry(name)
	if tModules[name] then
		tModules[name].close();
		tModules[name] = nil;
	end
end

function setDataFilter()
	bDataFilter = true;
	bTokenFilter = false;
	updateContents();
end

function setTokenFilter()
	bTokenFilter = true;
	bDataFilter = false;
	updateContents();
end

function updateContents()
	if bTokenFilter then
		title.setValue(Interface.getString("module_window_titletoken"));
	else
		title.setValue(Interface.getString("module_window_titledata"));
	end
	list.applyFilter();
end

function onModuleFilter(w)
	if bTokenFilter then
		if w.hastokens.getValue() == 0 then
			return false;
		end
	else
		if w.hasdata.getValue() == 0 then
			return false;
		end
	end
	if filter_open.getValue() == 1 then
		if not w.isActive() then
			return false;
		end
	end
	local sNameFilter = filter_name.getValue();
	if sNameFilter ~= "" then
		if not string.find(w.name.getValue():lower(), sNameFilter:lower(), 0, true) then
			return false;
		end
	end
	local sAuthorFilter = filter_author.getValue();
	if sAuthorFilter ~= "" then
		if not string.find(w.author.getValue():lower(), sAuthorFilter:lower(), 0, true) then
			return false;
		end
	end
	return true;
end

function setPermissions(s)
	for _,v in pairs(tModules) do
		v.setPermissions(s);
	end
end
