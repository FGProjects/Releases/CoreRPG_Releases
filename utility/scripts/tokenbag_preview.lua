-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sAssetName;
local sAssetType;

function setData(sAssetNameParam, sAssetTypeParam)
	sAssetName = sAssetNameParam;
	sAssetType = sAssetTypeParam;

	preview.setAsset(sAssetName);

	local bShowImport = ((sAssetType or "") == "image")
	button_import.setVisible(bShowImport);
end

function handleDrag(draginfo)
	if (sAssetType or "") ~= "" then
		draginfo.setType(sAssetType);
		draginfo.setTokenData(sAssetName);
		return true;
	end
end

function performImport()
	if (sAssetType or "") ~= "" then
		local nodeRecord = CampaignDataManager.createImageRecordFromAsset(sAssetName);
		if nodeRecord then
			local sDisplayClass = LibraryData.getRecordDisplayClass("image");
			Interface.openWindow(sDisplayClass, nodeRecord);
		end
		close();
	end
end
