-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local bInitialized = false;
local bUpdating = false;
local nodeSrc = nil;
local nDefault = 0;

function onInit()
	nodeSrc = window.getDatabaseNode();
	if nodeSrc then
		onUpdate();
		DB.addHandler(DB.getPath(nodeSrc, "toolbar"), "onUpdate", onUpdate);
		if nodeSrc.isReadOnly() then
			setVisible(false);
		end
	else
		setVisible(false);
	end
	bInitialized = true;
end
function onClose()
	if nodeSrc then
		DB.removeHandler(DB.getPath(nodeSrc, "toolbar"), "onUpdate", onUpdate);
	end
end
	
function onUpdate()
	if bUpdating then
		return;
	end
	bUpdating = true;
	local nValue = DB.getValue(nodeSrc, "toolbar", nDefault);
	if nValue == 0 then
		setValue(0);
	else
		setValue(1);
	end
	bUpdating = false;
end
function onValueChanged()
	if not bUpdating then
		bUpdating = true;
		if nodeSrc then
			DB.setValue(nodeSrc, "toolbar", "number", getValue());
		end
		bUpdating = false;
	end
	if bInitialized then
		notify();
	end
end

function notify()
	if window.parentcontrol and window.parentcontrol.window.onToolbarChanged then
		window.parentcontrol.window.onToolbarChanged();
	elseif window.onToolbarChanged then
		window.onToolbarChanged();
	end
end
