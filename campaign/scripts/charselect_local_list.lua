-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local sLabelLocal = Interface.getString("charselect_label_local");
	local sLabelCampaign = Interface.getString("charselect_label_campaign");

	local localIdentities = User.getLocalIdentities();
	for _, v in ipairs(localIdentities) do
		local w = createWindow(v.databasenode);
		if (v.session or "") == "" then
			w.source_label.setValue(sLabelLocal);
			w.source.setVisible(false);
		else
			w.source_label.setValue(sLabelCampaign);
			w.source.setValue(v.session);
		end
	end
	
	DB.addHandler("charsheet.*", "onAdd", addIdentity);
	DB.addHandler("charsheet.*.name", "onUpdate", onDetailsUpdate);
	DB.addHandler("charsheet.*.level", "onUpdate", onDetailsUpdate);
end

function onClose()
	DB.removeHandler("charsheet.*", "onAdd", addIdentity);
	DB.removeHandler("charsheet.*.name", "onUpdate", onDetailsUpdate);
	DB.removeHandler("charsheet.*.level", "onUpdate", onDetailsUpdate);
end

function addIdentity(nodeLocal)
	createWindow(nodeLocal);
end

function onDetailsUpdate(nodeField)
	local nodeChar = nodeField.getParent();
	local sID = nodeChar.getName();
	
	for _,w in pairs(getWindows()) do
		if w.id == sID then
			local sName, sDetails = GameSystem.getCharSelectDetailLocal(nodeChar);
			w.name.setValue(sName);
			w.details.setValue(sDetails);
			break;
		end
	end
end

function onListChanged()
	update();
end

function update()
	local bEditMode = (window.button_localedit.getValue() == 1);
	for _,w in pairs(getWindows()) do
		w.idelete.setVisibility(bEditMode);
	end
end
