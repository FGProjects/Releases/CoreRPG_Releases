-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local node = getDatabaseNode();
	
	if not node.isStatic() then
		registerMenuItem(Interface.getString("char_menu_delete"), "delete", 6);
		registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
		idelete.setVisible(true);
	end
	
	local portraitfile = User.getLocalIdentityPortrait(node);
	if portraitfile then
		portrait.setFile(portraitfile);
	end
	
	local sName, sDetails = GameSystem.getCharSelectDetailLocal(node);
	name.setValue(sName);
	details.setValue(sDetails);
end

function onMenuSelection(item, subselection)
	if item == 6 and subselection == 7 then
		deleteCharacter();
	end
end

function deleteCharacter()
	getDatabaseNode().delete();
end

function openCharacter()
	Interface.openWindow("charsheet", getDatabaseNode());
	windowlist.window.close();
end

function exportCharacter()
	CampaignDataManager.exportChar(getDatabaseNode());
end

