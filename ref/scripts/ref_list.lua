-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sFilter = "filter";
local sFilterValue = "";

function onInit()
	if altfilter then
		sFilter = altfilter[1];
	end
end

function updateFilter()
	local s = getFilter();
	if s ~= sFilterValue then
		sFilterValue = s;
		if sFilterValue == "" then
			setHeadersVisible(true);
			setPathVisible(true);
		else
			setHeadersVisible(false);
		end
	end
end

function getFilter()
	local vTop = window;
	while vTop.windowlist or vTop.parentcontrol do
		if vTop.windowlist then
			vTop = vTop.windowlist.window;
		else
			vTop = vTop.parentcontrol.window;
		end
	end
	if not vTop[sFilter] then
		return "";
	end

	return vTop[sFilter].getValue():lower();
end

function setHeadersVisible(bShow)
	local vTop = window;
	if vTop.showFullHeaders then
		vTop.showFullHeaders(bShow);
	end
	while vTop.windowlist or vTop.parentcontrol do
		if vTop.windowlist then
			vTop = vTop.windowlist.window;
		else
			vTop = vTop.parentcontrol.window;
		end
		if vTop.showFullHeaders then
			vTop.showFullHeaders(bShow);
		end
	end
end

function setPathVisible()
	setVisible(true);
	local vTop = window;
	while vTop.windowlist or vTop.parentcontrol do
		if vTop.windowlist then
			vTop.windowlist.setVisible(true);
			vTop = vTop.windowlist.window;
		else
			vTop.parentcontrol.setVisible(true);
			vTop = vTop.parentcontrol.window;
		end
	end
end

function onFilter(w)
	updateFilter();

	if sFilterValue == "" then
		return true;
	end
	
	local bShow = true;
	if w.keywords then
		local sKeyWordsLower = w.keywords.getValue():lower();
		for word in sFilterValue:gmatch("%w+") do
			if not sKeyWordsLower:find(word, 0, true) then
				bShow = false;
			end
		end
	else
		local sNameLower = w.name.getValue():lower();
		for word in sFilterValue:gmatch("%w+") do
			if not sNameLower:find(word, 0, true) then
				bShow = false;
			end
		end
	end

	if bShow then
		setPathVisible();
	end
	
	return bShow;
end
